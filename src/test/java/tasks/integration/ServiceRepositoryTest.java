package tasks.integration;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import tasks.model.Task;
import tasks.model.TasksOperations;
import tasks.model.taskList.ArrayTaskList;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
public class ServiceRepositoryTest {
    private Random rand = new Random();

    @Mock
    public Task _task;

    public Date date;

    public ArrayTaskList _repo= new ArrayTaskList();;

    public TasksOperations _service;

    public void setupListWithTasks(){
        date = new Date(Math.abs(rand.nextInt()));
        _task = mock(Task.class);

        _repo.add(_task);
        _service = new TasksOperations(FXCollections.observableArrayList(_repo.getAll()));
    }

    public void setupEmptyList(){
        date = new Date(Math.abs(rand.nextInt()));
        _repo = new ArrayTaskList();
        _service = new TasksOperations(FXCollections.observableArrayList(_repo.getAll()));
    }

    @Test
    public void whenAreNotIncomingTasks_shouldReturnEmptyList(){
        setupListWithTasks();
        ArrayList<Task> expectedResult = new ArrayList<Task>();

        ArrayList<Task> result = _service.incoming(date, date);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void whenRepoListIsEmpty_shouldReturnEmptyList(){
        setupEmptyList();
        ArrayList<Task> expectedResult = new ArrayList<Task>();

        ArrayList<Task> result = _service.incoming(date, date);

        Assertions.assertEquals(expectedResult, result);
    }

}
